import Vuex from "vuex";
import Cookie from "js-cookie";

const createStore = () => {
  return new Vuex.Store({
    state: {
      loadedPosts: [],
      token: null
    },
    mutations: {
      setPosts(state, posts) {
        state.loadedPosts = posts;
      },
      addPost(state, post) {
        state.loadedPosts.push(post);
      },
      editPost(state, editedPost) {
        const postIndex = state.loadedPosts.findIndex(
          post => post.id === editedPost.id
        );
        state.loadedPosts[postIndex] = editedPost;
      },
      setToken(state, token) {
        state.token = token;
      },
      clearToken(state) {
        state.token = null;
      }
    },
    actions: {
      nuxtServerInit(vuexContext, context) {
        return context.app.$axios
          .$get("/posts.json")
          .then(data => {
            const postArray = [];
            for (const key in data) {
              postArray.push({ ...data[key], id: key });
            }
            vuexContext.commit("setPosts", postArray);
          })
          .catch(e => console.log(context.error(e)));
      },
      setPosts(context, posts) {
        context.commit("setPosts", posts);
      },
      addPost(context, postData) {
        const createdPost = {
          ...postData,
          updatedDate: new Date()
        };
        return this.$axios
          .$post("/posts.json?auth=" + context.state.token, createdPost)
          .then(data => {
            context.commit("addPost", { ...createdPost, id: data.name });
          })
          .catch(e => console.log(e));
      },
      editPost(context, editedPost) {
        return this.$axios
          .$put(
            "/posts/" + editedPost.id + ".json?auth=" + context.state.token,
            editedPost
          )
          .then(msg => {
            context.commit("editPost", editedPost);
          })
          .catch(e => console.log(e));
      },
      authenticateUser(context, authData) {
        let authUrl =
          "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" +
          process.env.apiKey;
        if (!authData.isLogin) {
          authUrl =
            "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" +
            process.env.apiKey;
        }
        return this.$axios
          .$post(authUrl, {
            email: authData.email,
            password: authData.password,
            returnSecureToken: true
          })
          .then(res => {
            context.commit("setToken", res.idToken);
            localStorage.setItem("token", res.idToken);
            localStorage.setItem(
              "tokenExpiration",
              new Date().getTime() + Number.parseInt(res.expiresIn) * 1000
            );
            Cookie.set("token", res.idToken);
            Cookie.set(
              "tokenExpiration",
              new Date().getTime() + Number.parseInt(res.expiresIn) * 1000
            );
            return this.$axios.$post("http://localhost:3000/api/track-data", {
              data: "express authenticated"
            });
          })
          .catch(e => console.log(e));
      },
      initAuth(context, req) {
        let token;
        let tokenExpiration;
        if (req) {
          if (!req.headers.cookie) return;
          const cookieToken = req.headers.cookie
            .split(";")
            .find(c => c.trim().startsWith("token="));
          if (!cookieToken) return;
          token = cookieToken.split("=")[1];
          tokenExpiration = req.headers.cookie
            .split(";")
            .find(c => c.trim().startsWith("tokenExpiration="))
            .split("=")[1];
        } else if (process.client) {
          token = localStorage.getItem("token");
          tokenExpiration = localStorage.getItem("tokenExpiration");
        }
        if (new Date().getTime() > +tokenExpiration || !token) {
          context.dispatch("logout");
        }

        context.commit("setToken", token);
      },
      logout(context) {
        context.commit("clearToken");
        if (process.client) {
          localStorage.removeItem("token");
          localStorage.removeItem("tokenExpiration");
        }
        Cookie.remove("token");
        Cookie.remove("tokenExpiration");
      }
    },
    getters: {
      loadedPosts(state) {
        return state.loadedPosts;
      },
      isAuthenticated(state) {
        return state.token != null;
      }
    }
  });
};

export default createStore;
