import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _135049ca = () => import('..\\pages\\users.vue' /* webpackChunkName: "pages_users" */).then(m => m.default || m)
const _7b095910 = () => import('..\\pages\\users\\index.vue' /* webpackChunkName: "pages_users_index" */).then(m => m.default || m)
const _023e64a2 = () => import('..\\pages\\users\\_id\\index.vue' /* webpackChunkName: "pages_users__id_index" */).then(m => m.default || m)
const _5d2eee5a = () => import('..\\pages\\about\\index.vue' /* webpackChunkName: "pages_about_index" */).then(m => m.default || m)
const _65159bd1 = () => import('..\\pages\\admin\\index.vue' /* webpackChunkName: "pages_admin_index" */).then(m => m.default || m)
const _2a5b68cd = () => import('..\\pages\\posts\\index.vue' /* webpackChunkName: "pages_posts_index" */).then(m => m.default || m)
const _a9e54e78 = () => import('..\\pages\\admin\\new-post\\index.vue' /* webpackChunkName: "pages_admin_new-post_index" */).then(m => m.default || m)
const _30582069 = () => import('..\\pages\\admin\\auth\\index.vue' /* webpackChunkName: "pages_admin_auth_index" */).then(m => m.default || m)
const _25a14726 = () => import('..\\pages\\posts\\_id\\index.vue' /* webpackChunkName: "pages_posts__id_index" */).then(m => m.default || m)
const _84ba0bae = () => import('..\\pages\\admin\\_postId\\index.vue' /* webpackChunkName: "pages_admin__postId_index" */).then(m => m.default || m)
const _643320d8 = () => import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/users",
			component: _135049ca,
			children: [
				{
					path: "",
					component: _7b095910,
					name: "users"
				},
				{
					path: ":id",
					component: _023e64a2,
					name: "users-id"
				}
			]
		},
		{
			path: "/about",
			component: _5d2eee5a,
			name: "about"
		},
		{
			path: "/admin",
			component: _65159bd1,
			name: "admin"
		},
		{
			path: "/posts",
			component: _2a5b68cd,
			name: "posts"
		},
		{
			path: "/admin/new-post",
			component: _a9e54e78,
			name: "admin-new-post"
		},
		{
			path: "/admin/auth",
			component: _30582069,
			name: "admin-auth"
		},
		{
			path: "/posts/:id",
			component: _25a14726,
			name: "posts-id"
		},
		{
			path: "/admin/:postId",
			component: _84ba0bae,
			name: "admin-postId"
		},
		{
			path: "/",
			component: _643320d8,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
